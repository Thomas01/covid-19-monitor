import React from 'react';

//Components
import Cards from './components/Cards/Cards';
import Countries from './components/Countries/Countries';
import Charts from './components/Charts/Charts';
import { fetchData } from './api'

//Styles
import Styles from './App.module.css'

class App extends React.Component{
  //setting state
  state = {
    data: {},
    country: {}
  }

  //hundle country change
  handleCountryChange = async (country) => {
    const res = await fetchData(country)
    this.setState({data: res, country: country})

  }

  async componentDidMount(){
    const res = await fetchData()
    this.setState({data: res})
  }
    render(){
      const { data, country } = this.state;

      return(
        <div className = { Styles.container }>
        <div><h1>Covid-19 Tracker</h1></div>
          <Cards data = { data } />
          <Countries handleCountryChange={this.handleCountryChange} />
          <Charts data ={data} country={country}/>
        </div>
      )
    }
}

export default App;

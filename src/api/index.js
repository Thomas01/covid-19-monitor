import axios from 'axios';

const url = 'https://covid19.mathdro.id/api';

// fetch all data
export const fetchData = async(country) => {
    let countryUrl = url;

// fetch country data
    if (country){
        countryUrl = `${url}/countries/${country}`;
    }

    try{
        const { data: { confirmed, deaths, recovered, lastUpdate} } = await axios.get(countryUrl);
        const actualData ={ confirmed, deaths, recovered, lastUpdate, }
        return actualData;
    } catch(error) {
        console.log(error)
    }
}

// fetch daily data
export const fetchDailyInfo = async() =>{
    try{
        const {data} = await axios.get(`${url}/daily`);
        const modifieldData = data.map((dailyData) => ({
            confirmed: dailyData.confirmed.total,
            deaths: dailyData.deaths.total,
            date: dailyData.reportedData,
        }))

        return modifieldData;

    }catch(error){

        console.log(error);
    }
}

//fetch countries
export const fetchCountries = async () => {
    try{
        const {data: {countries}} = await axios.get(`${url}/countries`);
            return countries.map((country) => country.name);
    }catch(error){
        console.log(error)
    }
}
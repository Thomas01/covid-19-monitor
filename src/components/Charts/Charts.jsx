import React, {useState, useEffect} from 'react';
import { Line, Bar} from 'react-chartjs-2'

//api
import { fetchDailyInfo } from '../../api'

//styles
import styles from './Charts.module.css'

const Charts = ({data:{confirmed, recovered, deaths},country}) => {
    const [ dailyData, setDailyData ] = useState([]);

    useEffect(()=>{
        const fetchInfo = async() => {
            setDailyData(await fetchDailyInfo());
        }

        fetchInfo();
    }, [])
    const lineChart = (
        dailyData.length ? (
            <Line
            data = {{
            labels: dailyData.map(({ date }) => date),
            datasets: [{
                data: dailyData.map(({confirmed}) => confirmed),
                label: 'Infected',
                borderColor: 'blue',
                fill: true,
            },{
                data: dailyData.map(({deaths}) => deaths),
                label: 'Deaths',
                borderColor: 'red',
                backgroundColor: 'rgba(255, 0, 0, 0, 5)',
                fill: true,

            }]
        }}
        />

        ):null
    ) 
    
    const barChart = (
            confirmed ? (
                <Bar 
                data ={{
                    labels:['Confirmed', 'Recovered', 'Deaths'],
                    datasets: [{
                        label: 'People',
                        backgroundColor: [
                             '#0000ff',
                             '#008000',
                             '#ff0000',
                        ],
                        data: [confirmed.value, recovered.value, deaths.value]
                    }]
                }} 
                options = {{
                    legend: { display: true},
                    title: { display: true, text:`The current state in ${country}`},
                }}
               
            />
            ): null
    )

    return(
        <div className ={styles.container}>
            {country ? barChart : lineChart}
        </div>
    ) 
}

export default Charts;
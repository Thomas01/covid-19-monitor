import React, {useEffect, useState} from 'react';

//mui
import {NativeSelect, FormControl} from '@material-ui/core';

//styles
import styles from './Countries.module.css';

//components
import {fetchCountries} from '../../api'


const Countries = ({handleCountryChange}) => {
    const[pickCountry, setPickCountry] = useState([]);

useEffect(() => {
    const fetchData = async() => {
        setPickCountry(await fetchCountries());
    }
    fetchData()
}, [setPickCountry])

    return (
       <FormControl className={styles.FormControl}>
           <NativeSelect defaultValue ="" onChange ={(e) => handleCountryChange(e.target.value)} >
               <option value="">Global</option>
              {pickCountry.map((country, i) =><option key ={i} value ={country}>{country}</option>)}
           </NativeSelect>
       </FormControl>
    );
}

export default Countries;
import React from 'react';
import CountUp from 'react-countup';

//MUI
import { Card, CardContent, Typography, Grid} from '@material-ui/core';

//Styles
import Styles from './Cards.module.css';
import cx from 'classnames';


    const Cards = ({data: {confirmed, recovered, deaths, lastUpdate}}) =>{
    if(!confirmed){
        return "Loading..."
    }


    return(
        <div className={Styles.container}>
            <Grid container spacing={3} justify ="center">
                <Grid item component={Card} as={12} md={3} className = {cx(Styles.card, Styles.infected)}>
                    <CardContent>
                        <Typography color = "textSecondary" gutterBottom>
                        Number of Infections
                        </Typography>
                        <Typography variant="h5">
                            <CountUp
                            start = {0}
                            end = { confirmed.value }
                            duration = {2.5}
                            separator = ","
                            />
                        </Typography>
                        <Typography variant ="body2">
                            {new Date(lastUpdate).toDateString()}
                        </Typography>
                        <Typography color = "textSecondary">
                            Number of covid-19 confirmed cases globally
                        </Typography>
                    </CardContent>
                </Grid>

                <Grid item component={Card} as={12} md={3} className = {cx(Styles.card, Styles.recovered)}>
                    <CardContent>
                        <Typography color = "textSecondary" gutterBottom>
                            Recovered
                        </Typography>
                        <Typography variant="h5">
                            <CountUp
                            start = {0}
                            end = { recovered.value }
                            duration = {2.5}
                            separator = ","
                            />
                        </Typography>
                        <Typography variant ="body2">
                            {new Date(lastUpdate).toDateString()}
                        </Typography>
                        <Typography color = "textSecondary">
                            Number of recovered patients 
                        </Typography>
                    </CardContent>
                </Grid>

                <Grid item component={Card} as={12} md={3} className = {cx(Styles.card, Styles.deaths)}>
                    <CardContent>
                        <Typography color = "textSecondary" gutterBottom>
                            Deaths
                        </Typography>
                        <Typography variant="h5">
                            <CountUp
                            start = {0}
                            end = { deaths.value }
                            duration = {2.5}
                            separator = ","
                            />
                        </Typography>
                        <Typography variant ="body2">
                            {new Date(lastUpdate).toDateString()}
                        </Typography>
                        <Typography color = "textSecondary">
                            Number of deaths cased by covid-19
                        </Typography>
                    </CardContent>
                </Grid>
            </Grid>

        </div>
    );
}

export default Cards;